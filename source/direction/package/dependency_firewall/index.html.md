---
layout: markdown_page
title: "Category Direction - Dependency Firewall"
description: "By preventing the introduction of security vulnerabilities further upstream, organizations can let their development teams work faster and more efficiently."
canonical_path: "/direction/package/dependency_firewall/"
---

- TOC
{:toc}

## Dependency Firewall

Many projects depend on packages that may come from unknown or unverified providers, introducing potential security vulnerabilities.  GitLab already provides [dependency scanning](https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#supported-languages-and-package-managers) across a variety of languages to alert users of any known security vulnerabilities, but we currently do not allow organizations to prevent those vulnerabilities from being downloaded to begin with.

The goal of this category will be to leverage the [dependency proxy](https://about.gitlab.com/direction/package/dependency_proxy/), which proxies and caches dependencies, to give more control and visibility to security and compliance teams. We will do this by allowing users to create and maintain an approved/banned list of dependencies, providing more insight into the usage and impact of external dependencies and by ensuring the [GitLab Security Dashboard](https://docs.gitlab.com/ee/user/application_security/security_dashboard/) is the single source of truth for all security related issues.

By preventing the introduction of security vulnerabilities further upstream, organizations can let their development teams work faster and more efficiently.

- [Maturity plan](#maturity-plan)
- [Issue list](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ADependency%20Firewall)
- [Overall vision](/direction/ops/#package)
- [Research issues](https://gitlab.com/groups/gitlab-org/-/boards/1397751?scope=all&utf8=%E2%9C%93&label_name[]=group%3A%3Apackage&label_name[]=Category%3ADependency%20Firewall)

This page is maintained by the Product Manager for Package, Tim Rizzi ([E-mail](mailto:trizzi@gitlab.com))

## Use cases

- Verify package integrity from one single place. See what has been changed and test them for security vulnerabilities (part of black duck model). 
- Filter the available upstream packages to include only approved, whitelisted packages.
- Delay updates from packages that have been recently updated under suspicious circumstances. For example, you will be able to delay any packages in which the following circumstances have occurred:
  - Author change
  - Author information change
  - Programming language change 
  - Activity after a long period of inactivity
  - Large code changes
  - [Introduction of an executable](https://blog.reversinglabs.com/blog/mining-for-malicious-ruby-gems)
  - [Executable files with a non-executable extention like .png](https://blog.reversinglabs.com/blog/mining-for-malicious-ruby-gems)
  - Name very similar to a popular package (typosquatting)
- Audit and mirror every dependency to ensure you are running and requiring Developers to take an active, documented role in vetting external dependencies.
  
## What's next & why

Today, if you try to install an npm package from the GitLab Package Registry and it's not found, GitLab will forward the request to the npm public registry. As an MVC of the Dependency Firewall, GitLab will flag any known vulnerabilities or suspicious activity prior to the package being downloaded. In order to accomplish this, we must update the architecture of the feature to store package metadata. 

[gitlab-#241239](https://gitlab.com/gitlab-org/gitlab/-/issues/241239), which will ensure that when a package is pulled through the proxy that GitLab stores the packages metadata, so that we can flag suspicious packages based on that metadata. 

Once the above is complete, [gitlab-#215393](https://gitlab.com/gitlab-org/gitlab/-/issues/215393) will flag any npm packages that are pulled through the Dependency Proxy that recently had the author name or email updated to ensure that users are aware of any suspicious changes.

## Maturity Plan

This category is currently at the "Planned" maturity level, and
our next maturity target is "Minimal" (see our [definitions of maturity levels](/direction/maturity/)).

For a list of key deliverables and expected outcomes, check out the epic, [gitlab-#5133](https://gitlab.com/groups/gitlab-org/-/epics/5133), which includes links and expected timing for each issue.

## Competitive landscape

* [JFrog X-Ray](https://jfrog.com/xray/)
* [Nexus](https://www.sonatype.com/nexus-firewall)
* [GitHub Package Registry](https://github.com/features/package-registry) 

JFrog utilizes a combination of their Bintray and XRay products, to proxy, cache and screen dependencies. They also provide dependency graphs across multiple languages and centralized dashboards for the review and remediation of vulnerabilities. It is a mature product, that is generally well received by users. 

GitHub's new package registry does a really nice job of creating visibility into the dependency graph for a given package, but they do not give users the ability to control which packages are used in a given group/project. 
​​
## Top Vision Item(s)

Our top vision item is [gitlab-ee#13761](https://gitlab.com/gitlab-org/gitlab-ee/issues/13761) which will introduce the concept of approved and banned dependencies to Gitlab. 
