---
features:
  primary:
  - name: "Distributed Reads for Gitaly Cluster"
    available_in: [premium, ultimate]
    gitlab_com: false
    documentation_link: 'https://docs.gitlab.com/ee/administration/gitaly/praefect.html#distributed-reads'
    image_url: '/images/unreleased/gitaly-distributed-read.png'
    reporter: mjwood
    stage: create
    categories:
    - Gitaly
    issue_url: 'https://gitlab.com/gitlab-org/gitaly/-/issues/3334'
    description: |
      Users want the highest possible levels of fault tolerance in highly-available Gitaly Clusters. This release introduces distributed reads for Gitaly Clusters. Instead of always reading from the primary node, Gitaly now automatically distributes read requests to any up-to-date node in the cluster. Distributing requests maximizes both performance and value proposition by giving your users access to all the computing resources provisioned in the cluster.
      Users also want to prevent performance degradation from noisy-neighbor repositories on the same node. This issue is particularly important for large and busy monorepos, where thousands of engineers and CI pipelines simultaneously access the same Git repository. By horizontally distributing read requests, Gitaly Clusters improve performance across all repositories in the cluster.
