---
features:
  secondary:
  - name: "Designate optional sections in the codeowners file"
    available_in: [premium, ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/project/code_owners.html#optional-code-owners-sections)'
    reporter: danielgruesso
    stage: create
    categories:
    - Source Code Management
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/232995'
    description: |
      [Code owner sections](https://docs.gitlab.com/ee/user/project/code_owners.html#code-owners-sections) allow multiple teams to configure their own code owners independently in the same file. This means teams can be assigned as code owners in their own section so that each team is added as a distinct reviewer in merge requests. This helps when multiple teams are responsible for common parts of the code, by helping authors getting feedback from the right reviewers. However, when approval from Code Owners is required, this applies to all the sections meaning all teams need to approve, which can hold back a changes from getting merged.
      
      GitLab 13.8 introduces the ability to designate optional sections in your code owners file. Simply prepend the section name with the caret `^` character and the section will be treated as optional. This means, related change through merge requests will not require approval from designated owners. With optional sections you may continue to designate responsible parties for various parts of the code while providing a more relaxed policy for parts of your project that may be updated often but don't require stringent reviews.
