---
layout: handbook-page-toc
title: "GitLab Security Certifications and Attestations"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## GitLab Security Certifications and Attestations
In support of our ongoing commitment to information security and transparent operations, the GitLab Security Compliance team is dedicated to obtaining and maintaining industry recognized security and privacy third party certifications and attestations. The benefits from these activities include:

For customers:
* increases visibility and confidence in our information security program
* increases ease in onboarding and managing GitLab as a vendor

For GitLab:
* ensures we are meeting all requirements of a strong and comprehensive information security program aligned with industry best practices
* enables our field teams to quickly share the state of our security program with potential and existing customers
* reduces the need for GitLab's security team to fill out individual customer security questionnaires or assessments

### Current
* [SOC 2 Type 1 Report](https://www.aicpa.org/interestareas/frc/assuranceadvisoryservices/aicpasoc2report.html) - Security Criteria
   * The SOC2 Type 1 report available for customer and potential customers upon request is scoped to GitLab.com. There are elements of the report that cover organizational-level security considerations (e.g., Business Continuity Planning, Risk Assessments, etc.) which go beyond the scope of GitLab.com as a SaaS product and speak to the mature state of GitLab's information security program.
* [PCI DSS SAQ-A](https://www.pcisecuritystandards.org/pci_security/completing_self_assessment) Self-Assessment 
   * GitLab partners with PCI-compliant credit card processors in order to ensure adequate protections of payment processing information. 
* [CSA Consensus Assessments Initiative Questionnaire v3.0.1](https://cloudsecurityalliance.org/star/registry/gitlab/) Self-Assessment
* [Annual Third Party Penetration Test](https://about.gitlab.com/handbook/engineering/security/#annual-3rd-party-security-testing)

### Planned (Roadmap)
Year(s): 2020/2021
* [SOC 2 Type 2 Report](https://www.aicpa.org/interestareas/frc/assuranceadvisoryservices/aicpasoc2report.html) - Security Criteria
* [SOC 3 Report](https://www.aicpa.org/interestareas/frc/assuranceadvisoryservices/aicpasoc3report.html)

Year(s): 2021/2022
* [SOC 2 Type 2 Report](https://www.aicpa.org/interestareas/frc/assuranceadvisoryservices/aicpasoc2report.html) - +Confidentiality Criteria
* [ISO/IEC 20243-1:2018](https://www.iso.org/standard/74399.html) Self-Certification 

### In Consideration
The following security certifications and attestations are currently on our roadmap for consideration and have not yet been formally committed or contracted: 
* [ISO/IEC 27001:2013](https://www.iso.org/isoiec-27001-information-security.html) Certification
* [FedRAMP Moderate](https://www.fedramp.gov/understanding-baselines-and-impact-levels/) Certification
* [SOC 2 Type 2 Report](https://www.aicpa.org/interestareas/frc/assuranceadvisoryservices/aicpasoc2report.html) +Availability Criteria




## Requesting a copy of the GitLab SOC2 Type 1 report

The nature of SOC2 reports is such that these reports cannot be made publicly available. Not only do these reports contain very detailed information about how our systems operate (which could make a potential attack against GitLab easier) but these reports also contain proprietary information about how these audit firms conduct their testing. For these reasons we can only share SOC2 reports with prospective customers that are under an NDA with GitLab or with current customers bound by the confidentiality of our customer agreements. The report should not be shared with anyone other than the individual requestor(s).
- See the full [process for requesting a SOC2 Report from GitLab](/handbook/engineering/security/security-assurance/risk-field-security/procedures/SOC2.html)




