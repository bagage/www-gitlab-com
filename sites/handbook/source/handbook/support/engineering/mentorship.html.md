---
layout: handbook-page-toc
title: Support Engineering Mentorship
description: This page covers the expectations of Senior+ Engineers and how they can engage in mentorship at GitLab
---

### Expectations of a mentee

As a mentee, it's important to remember the following:
- This is a learning opportunity, not a 'hand-off'. The Mentor should not take over the issue, but instead support and guide you to learn. 
- Try to state the problem clearly, and do at least 15 minutes of prep work beforehand to help speed up troubleshooting.
- If the mentor is moving to fast or not explaining clearly enough, let them know. It could be scary, but they should understand.
- Your mentor is human too. They may not know the answer as well, but hopefully through pairing, you both could get closer to the right answer.


### Expectations of a mentor

#### How to set up mentoring sessions
Anyone in Support Engineering can act as a mentor, but this page is specifically about how Senior+ Engineers can set up and be intentional about mentoring others. Through a [trial in November 2019](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/2704) we determined the following guidelines:

- Senior Engineers should aim to have 5 pairing sessions a week. This is not a limit or even a floor, but an intention. Some weeks it could be higher, and others lower, but if so, do so with intention.
- They can be scheduled or as issues arise.
- Senior Engineers can hold office hours as a form of mentorship.
- If you see a ticket where you are an expert, and can offer help, consider pairing with that engineer.
- You can use #support-donut to find possible engineers to pair with if no-one springs to mind.

We've started with these simple guidelines to help outline ways you can connect with others.

#### How to engage in mentoring

As a mentor it's important to remember:
- explain your thought process as clearly as possible. 
- Seek to land on a common understanding so that it's clear that you both understand each other.
- Try to avoid using "shortcuts" or "simplifications" unless you and the mentee agree there is a common understanding.
- Empathy is key. Meet the engineer where they are, and support them as we would a customer.

