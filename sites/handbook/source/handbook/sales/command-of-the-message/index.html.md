---
layout: handbook-page-toc
title: "Command of the Message"
description: "Command of the Message is GitLab's customer value-based sales messaging framework and methodology" 
---

{::options parse_block_html="true" /}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

Force Management's definition of Command of the Message is "being audible ready to define your solutions to customers’ problems in a way that differentiates you from your competitors and allows you to charge a premium for your products & services.” Critical sales skills to demonstrate Command of the Message include:
*  Uncover Customer Needs
*  Articulate Value and Differentiation
*  Negotiate Value

## Customer Value Drivers

Value drivers describe what organizations are likely proactively looking for or needing and are top-of-mind customer topics that exist even if GitLab doesn’t. Value drivers may cause buyers to re-allocate discretionary funds, and they support a value-based customer conversation. Organizations adopt and implement GitLab for the following value drivers:
1.  **Increase Operational Efficiencies** - _simplify the software development toolchain to reduce total cost of ownership_
1.  **Deliver Better Products Faster** - _accelerate the software delivery process to meet business objectives_
1.  **Reduce Security and Compliance Risk** - _simplify processes to comply with internal processes, controls and industry regulations without compromising speed_

## Resources: Core Content

| **Asset Title** | **Why / When Use** |
| ------ | ------ |
| [GitLab Value Framework](https://docs.google.com/document/d/1ZNWZ46bAp_Ii1UCl5H1THj6cJehh3qsCGJbZXh9yc9g/edit?usp=sharing) | primary asset with in-depth information on GitLab's value drivers and defensible differentiators |
| [GitLab Value Framework Summary](https://drive.google.com/open?id=1l2g7OJ3mIrCgUlLvwQbtwo-2Eg1lV8rkqUTy9dEoLdc) | 1-page quick guide, also great for sharing with internal champions |
| [GitLab CXO Digital Transformation Discovery Guide](https://drive.google.com/open?id=1balLINV-vnd6-6TYzF3SIIJKrBPUQofVSDgQ5llC2Do) | 3-page conversation guide for executive discussions |
| [Proof Points](/handbook/sales/command-of-the-message/proof-points/) | don't take our word for it...3rd party validation from customers, analysts, industry awards, and peer reviews |
| [GitLab Customer Use Case Solution Summary](https://docs.google.com/document/d/11O13xs0KABA7RSrVlF_Qm9kDRJGtK-LmvJlVGYr0qNM/edit?usp=sharing) | quick reference guide highlighting how GitLab is uniquely positioned to address key software development customer use cases |
| [Customer deck based on value drivers](https://docs.google.com/presentation/d/1SHSmrEs0vE08iqse9ZhEfOQF1UWiAfpWodIE6_fFFLg/edit?usp=sharing) | alternative to the standard GitLab pitch deck that starts with the customer's perspective and aligns to the 3 value drivers (video [here](https://youtu.be/UdaOZ9vvgXM)) |
| [Command Plan](/handbook/sales/command-of-the-message/command-plan/) | how GitLab has operationalized Command of the Message and MEDDPPICC into the opportunity management process |
| [MEDDPPICC](/handbook/sales/meddppicc/) | proven methodology used for strategic opportunity management and complex sales process orchestration for enterprise organizations | 
| [Demystifying the Metrics Conversation](/handbook/sales/command-of-the-message/metrics/) | tips and tricks for navigating the Metrics conversation with customers |

## GitLab Differentiators

Differentiators can influence a technical buyer's decision criteria in defining the required capabilities for the solution they are seeking. Effective differentiators must be perceived as valuable by the customer and be defensible.

<details>
<summary markdown="span">Single Application for Entire DevOps Lifecycle</summary>

-  Complete DevSecOps platform delivered as a single application<br>
-  One interface, one user-model, one data model<br>
-  A software ‘factory’ that supports the entire DevOps lifecycle<br>
-  Deeply integrated, making developers happier and more efficient<br>
-  Centralized collaboration for a wide variety of roles (collaborate without waiting)<br>
</details>

<details>
<summary markdown="span">Leading SCM and CI in One Application</summary>

-  The backbone of a DevOps toolchain in one application<br>
-  Streamlines code review and collaboration<br>
-  Start with SCM or CI (or both)<br>
-  One interface, one user-model, one data model<br>
-  Interoperable with other tools (continue using tools you love!)<br>
-  Proven enterprise scale<br>
</details>

<details>
<summary markdown="span">Built In Security and Compliance</summary>

-  Security features out-of-the-box (code scanning, dependency scanning, secrets detection, etc.)<br>
-  Automated security testing and audit controls to facilitate policy compliance<br>
-  Shift left: move security testing earlier in the development lifecycle; security testing at the point of code commit<br>
-  Developers get immediate feedback about new vulnerabilities they introduce<br>
-  Enables better collaboration between development and security teams<br>
-  One interface, one user-model, one data model for DevSecOps<br>
</details>

<details>
<summary markdown="span">Deploy Your Software Anywhere</summary>

-  Deploy to any environment, any cloud<br>
-  Support for GCP, AWS, Azure, OpenShift, VMWare, On Prem, Bare Metal, etc.<br>
-  Workflow portability: one deployment workflow regardless of destination<br>
</details>

<details>
<summary markdown="span">Optimized for Kubernetes</summary>

-  Enables adoption of modern cloud-native development patterns such as microservices and serverless<br>
-  Minimizes the Kubernetes learning curve by enabling setup and use of clusters from the GitLab user interface<br>
-  View and manage Kubernetes deployment pod details and logs from the GitLab user interface<br>
-  Automatic monitoring of every Kubernetes deployed application and the clusters they are deployed to<br>
-  Built-in canary and incremental deployment strategies with automatic canary metric collection and display<br>
-  Active contributor to the future of cloud native technologies (through the Cloud Native Computing Foundation)<br>
</details>

<details>
<summary markdown="span">End-to-End Insight and Visibility</summary>

-  Common data model uniquely allows for insights across the entire DevSecOps lifecycle<br>
-  Configurable insights dashboard that shows status of work items over time<br>
-  Cycle analytics data helps identify areas of improvement of cycle times<br>
-  Security insights provide a roll-up of vulnerabilities<br>
-  Program level insights help keep projects on-track<br>
</details>

<details>
<summary markdown="span">Flexible GitLab Hosting Options</summary>

-  Use in the cloud or install in your own environment (cloud or on-prem)<br>
-  Feature parity between GitLab.com and self-managed<br>
-  Migrate from one hosting option to another when needs change<br>
-  Implement hybrid hosting models to support scaling (like CI Runners in the cloud)<br>
-  Easy to upgrade and maintain in any environment<br>
</details>

<details>
<summary markdown="span">Rapid Innovation</summary>

-  Releases every month, for 100+ months running<br>
-  A constant stream of new features --> GitLab gets more valuable over time<br>
-  Uniquely transparent product development process; customers, partners and community can contribute<br>
</details>

<details>
<summary markdown="span">Open Source; Everyone Can Contribute</summary>

-  Open core development model allows anyone to contribute to the functionality of the product<br>
-  Uniquely transparent product development process engaging customers, partners and the community<br>
-  Strong and growing community (100K+ organizations and millions of users)<br>
-  Harness open source innovations within the product experience (eg. Prometheus)<br>
</details>

<details>
<summary markdown="span">Collaborative and Transparent Customer Experience</summary>

-  We operationalize our core values into how we develop our product and engage with our community and customers<br>
-  Extreme transparency around roadmaps, issues, company meetings and processes<br>
-  We use GitLab for collaborating with customers in a transparent way, for both issue resolution and roadmap iteration<br>
-  GitLab provides an evolving case study of DevSecOps and agile practices at scale, accessible through the public handbook<br>
</details> 

## Additional Resources

<details>
<summary markdown="span">Command of the Message Mantra</summary>

The Mantra is a framework you can use to clearly demonstrate that you have a complete understanding of your customer's goals, needs, and metrics for success. It also provides you with a customer-focused context to transition from the customer's needs which you have clearly articulated into how GitLab helps meet those needs. The Mantra is also a very good meeting preparation tool to determine how well you know your customer. If you cannot clearly articulate a customer-specific mantra, then you are not ready to progress the deal and you need to do more research. Below is a breakdown of the CoM Mantra framework which can be adjusted and articulated in your own words.

*  What I hear you saying Mr./Ms. Customer is that these are the **Positive Business Outcomes** you’re trying to achieve...
*  In order to achieve these positive business outcomes, we agreed that these are the **Required Capabilities** you’re going to need...
*  And you’ll probably want to measure these required capabilities using these **Metrics** 
*  Let me tell you **How We Do It...**
*  Let me tell you **How We Do It Better/Differently...**
*  But don’t take my word for it...**(Proof Points)**
</details>

<details>
<summary markdown="span">Job Aids</summary>

*  Overview resources
   - Comprehensive [GitLab CoM & MEDDPICC training slide deck](https://drive.google.com/open?id=1bWdV__GwN9WzkidBc0qMFu1GGln3rf5C) (prior to transition to MEDDPPICC)
   - [GitLab CoM & MEDDPICC Participant Guide](https://drive.google.com/open?id=1qSn-PZJ9_mk-dhnRY01BdoeBcrtC7jVr) (prior to transition to MEDDPPICC)
*  Prepare
   - [Pre-Call Plan](https://docs.google.com/document/d/1yjyfvMoDvayZca5hXiIwSHYc9T1M3mTc7ocqzjhqOf8/copy)
*  Discovery
   - [Customer Call Notes Template](https://docs.google.com/document/d/1hlLvfgQMgQS5g2ykEc6eNZP_wZd1M8GSmS-JsN_vICU/copy)
      - You may also choose to utilize the [Role Play Notes](https://docs.google.com/document/d/185a4mI3HMFnV_l6NwrsAndduFFlTvm5tPiIuPVy0ONQ/copy) template
*  Qualify
   - [Capturing "MEDDPPICC" Questions for Deeper Qualification](/handbook/sales/#capturing-meddppicc-questions-for-deeper-qualification)
   - [MEDDPPICC training slides](https://drive.google.com/open?id=1i3D64esfBitwn1ZXKB1-yjs52Z5hMsUggVClUKTcqjk)
   - [MEDDPPICC template](https://docs.google.com/document/d/1WbHoSL4r7S553n90sAEVuSdBNImWfCk3vTJINw2ud8A/copy)
   - [Opportunity Qualifier](https://docs.google.com/document/d/1Tz6bQKD4Ff2-XqpSXRQslD8yvrphwXaL6oEl74DAjeQ/copy)
*  Role Play materials
   - [Role Play Prep Sheet](https://docs.google.com/document/d/1nQ2yH4hg_btFi5XGHhvDjNh9-TKgxAYGO-bLYl8cMdc/copy)
   - [Role Play Notes](https://docs.google.com/document/d/185a4mI3HMFnV_l6NwrsAndduFFlTvm5tPiIuPVy0ONQ/copy)
*  Check out and subscribe to The Audible-Ready Podcast (from Force Management) for insights to help improve your productivity, generate more revenue, and increase competitive win rates on your favorite podcast player
   - [iTunes](https://podcasts.apple.com/us/podcast/the-audible-ready-podcast/id991362894)
   - [SoundCloud](https://soundcloud.com/force-management-1)
   - [Sticher](https://www.stitcher.com/podcast/the-audible-ready-podcast)
   - [Spotify](https://open.spotify.com/show/2JMvuitWVC34R2Kw7fHFDN)
   - [Google Podcast](https://podcasts.google.com/?feed=aHR0cHM6Ly9hdWRpYmxlcmVhZHlwb2RjYXN0LmxpYnN5bi5jb20vcnNz&ved=0CAAQ4aUDahcKEwjA2e2koZXoAhUAAAAAHQAAAAAQCQ)
*  GitLab sales managers may access additional information in the [Force Management Command Center](https://gitlab.lyearn.com/) (password protected since resources contain Force Management intellectual property). In particular, the [Channels](https://gitlab.lyearn.com/#/learner/channels) section of the Force Management Command Center contains supplemental instructional videos, podcasts, and blogs).
</details>

<details>
<summary markdown="span">Sales Manager Materials</summary>

*  Slides for Managers
     - [GitLab Manager Coaching slide deck](https://drive.google.com/open?id=1xxWlYd-YoRa51B5AD1LAdl3x5DsXBxfx) (Aug 2019)
     - [Manager Certification: Training slides & Coaching Best Practices](https://drive.google.com/open?id=1SlbTZf-vuucTIgP757qmF7pBUORPmSLD) (Feb 2020)
     - [Manager Certification: Core Command of the Message concepts for teach back](https://drive.google.com/open?id=1GooZccF_FUUtCDk0qHEfK5NRWBVJrSXq) (Feb 2020)
*  [Manager Coaching & Reinforcement Playbook](https://drive.google.com/open?id=1perfP59qxJlEs9AEkoPt4su23Jwk7IqK) (Feb 2020)
*  [Opportunity Consults Handbook page](/handbook/sales/command-of-the-message/opportunity-consults)
*  [Opportunity Coaching Guide](https://docs.google.com/document/d/1IZA9Fo2SvZOrtUVpXOjwwqs76lKdXFs4hTezbxRq5v8/copy) (Aug 2019)
*  [Coaching & Reinforcement Template](https://drive.google.com/file/d/0B-mC9VLTLN0bTWRUdFlKbVlpUVlia05wOGJQcUNwUVA2elpz/view?usp=sharing) (Feb 2020) 
*  [GitLab CoM & MEDDPICC Fast Start Program Manager Playbook](https://drive.google.com/open?id=1n76gU6whKW51ixMfFvgXoGsq512PsCHG) (Aug 2019) (prior to transition to MEDDPPICC)
*  Featured Command Center training modules (note: course access requires Force Management Command Center license)
     - [Skill/Will Model course](https://gitlab.lyearn.com/#/learner/courses/5b0ec54911bac00011139811/card) (40 minutes)
     - [Coaching Model course](https://gitlab.lyearn.com/#/learner/courses/5b0edb16c6b58400126eb350/card) (39 minutes)
     - Opportunity Coaching Best Practices Series
          - [Opportunity Coaching powered by MEDDPICC](https://gitlab.lyearn.com/#/learner/courses/5b32b9337f8a9800195a8288/card) (21 minutes) (note: does not include 2nd P for Partners)
          - [Selecting the Right Deals](https://gitlab.lyearn.com/#/learner/courses/5e8f67688ab5f033dace51ab/card) (5 minutes)
          - [Establishing a Cadence](https://gitlab.lyearn.com/#/learner/courses/5e8f675b8ab5f087b4ce5197/card) (5 minutes)
          - [Preparing the Seller](https://gitlab.lyearn.com/#/learner/courses/5e8f674a8ab5f06592ce5183/card) (10 minutes)
          - [Preparing Yourself](https://gitlab.lyearn.com/#/learner/courses/5e8f673e8ab5f0f4e2ce514b/card) (17 minutes)
          - [Set the Right Tone and Structure](https://gitlab.lyearn.com/#/learner/courses/5e8f67346ebfcb03b8af93af/card) (10 minutes)
          - [Common Gaps and Adding Value](https://gitlab.lyearn.com/#/learner/courses/5e8f67298ab5f0726cce5119/card) (35 minutes)
          - [Document for Impact](https://gitlab.lyearn.com/#/learner/courses/5e8f671e8ab5f01212ce5105/card) (5 minutes)
</details>

<details>
<summary markdown="span">Sharing Feedback</summary>

Over time, the GitLab Value Framework and associated content above will iterate and evolve. To ensure these changes are easily consumable, iterations will be made on a predictable cadence. We will start with a quarterly cadence. 
*  At the start of the last month of a fiscal quarter, Field Enablement will announce a Call for Feedback to solicit input/feedback on suggested iterations and improvements to the GitLab Value Framework and associated content (but feedback may be submitted at any time)
*  To share feedback, submit an issue using [**this issue template**](https://gitlab.com/gitlab-com/sales-team/field-operations/enablement/-/issues/new?issuable_template=value-framework-feedback)  
*  Field Enablement and Product Marketing leadership will review feedback during the middle of the first month of a new quarter. Additional stakeholders and subject matter experts will be pulled in as appropriate.
*  During this review, Field Enablement and Product Marketing will determine the disposition for each issue submitted with three possible outcomes
   1. Accepted (label: `vff::accepted`) - Value Framework feedback that will be actioned on
   1. Deferred (label: `vff::deferred`) - Value Framework feedback that will be deferred until more information is gathered
   1. Declined (label: `vff::declined`) - Value Framework feedback that is declined (no action will be taken)
*  When the `vff::accepted` label is added, a version label will be applied (`ver::1.1`, `ver::1.2`, etc.) to indicate the version of the Value Framework in which the improvement will be implemented
*  When the improvement has been implemented, the `vff::completed` label will be applied 
*  Around the end of the first month of a new quarter, a summarized update of accepted feedback will be shared with the field and implementation of those updates will be tracked in the issues
</details>

<details>
<summary markdown="span">Value Framework Iteration</summary>

Below is a summary of enhancements made to the GitLab Value Framework and CoM materials since the original version 1.0 launch in Aug 2019.
 
**Version 1.1 (May 2020)**
- Created this stand-alone CoM Handbook page
- Exposed mantra, GitLab value drivers, and differentiators in Handbook
- Adjusted [core content](/handbook/sales/command-of-the-message/#resources-core-content) sharing settings to make available for public consumption
- Integrated Professional Services value proposition into the GitLab Value Framework
- Introduced [Discovery Questions for Selling GitLab Premium/Silver and Ultimate/Gold](/handbook/sales/qualification-questions/#questions-for-selling-gitlab-premiumsilver-and-ultimategold)
- Updated proof points

**Version 1.2 (Aug 2020)**
- More Proactive and Robust Metrics Guidance via [Demystifying the Metrics Conversation](/handbook/sales/command-of-the-message/metrics/) Handbook page
- Changed MEDDPICC to [MEDDPPICC](/handbook/sales/meddppicc/) to reflect importance of understanding Partner influence on opportunities
- MVP release of the [GitLab Customer Use Case Solution Summary](https://docs.google.com/document/d/11O13xs0KABA7RSrVlF_Qm9kDRJGtK-LmvJlVGYr0qNM/edit?usp=sharing) featuring Tier 1 customer use cases
- Content refresh (updated [Proof Points](/handbook/sales/command-of-the-message/proof-points/), new ROI materials, [Discovery and Trap-Setting Questions for Selling GitLab Premium/Silver and Ultimate/Gold](/handbook/sales/qualification-questions/#questions-for-selling-gitlab-premiumsilver-and-ultimategold), and Peach Tech & Fuzz It related updates)
</details>
