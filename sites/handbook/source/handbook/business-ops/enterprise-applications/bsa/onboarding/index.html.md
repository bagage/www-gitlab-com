---
layout: handbook-page-toc
title: "Business Systems Analysts Onboarding"
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

Awesome! You're about to become a Business Systems Analyst at GitLab!
Make sure you've checked out our [handbook](/handbook/) beforehand, so you get a feeling of how we work at GitLab. Below you'll find everything you need to start your journey.

You will feel very slow in the beginning, which is perfectly normal. There is a lot of information being thrown at you all at once. Your goal for the first few weeks here at GitLab is simply to listen, absorb, and ask as many questions as possible.

> If you haven't already, please read the main section of the [Business Operations Handbook](/handbook/business-ops/) and the [Business Systems Analysts](/handbook/business-ops/enterprise-applications/bsa/) section.

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


## <i class="far fa-building" id="biz-tech-icons"></i> Business Systems Analysts Onboarding

### I just got here, now what? - GitLab Buddy!

You will be assigned a Buddy to help you find your way around. Your buddy will schedule a coffee chat with you during your first week. To get you up to speed so you can start contributing to the team efforts and improving GitLab as a product, you will be assigned to an onboarding issue that provides a few tips on how to navigate around the team resources as well as company resources.

Your Buddy will also be assigned to your first few milestones issues alongside you. While you should feel free to ask anyone for help at anytime, your buddy is a dedicated person you can rely on for help and guidance.

#### GitLab buddy responsibilities

As a buddy, part of your responsibility is to guide the new team member around the department, as well as facilitating a smooth ramp-up within the rest of the group whenever necessary. We created a [Google Docs with a template agenda](https://docs.google.com/document/d/1X88hp9eW_aIIVq5JIvzIg9-oHU_TwXGTA670Av-fYso/edit?usp=sharing) you can use to structure your 1:1s. You should feel free to structure those calls whenever you and the new joiner feel like it. Read more about [general buddy responsibilities](/handbook/people-group/general-onboarding/onboarding-buddies/) in our handbook.

### GitLab Trainings

#### Business Operations onboarding training
Much of the training BSAs should consider is already part of our onboarding. As time progresses, we may add additional things, so a periodic review of [Business Operations department](https://gitlab.com/gitlab-com/people-group/employment-templates/-/blob/master/.gitlab/issue_templates/onboarding_tasks/department_business_operations.md) onboarding activities for new actions is valuable.

#### Diversity, Inclusion, and Belonging Training
[Diversity, Inclusion & Belonging](/handbook/values/#diversity-inclusion) is one of GitLab's 6 values. It's what we believe distinguishes us from other companies. Business Systems Analysts should take [DIB Training](https://gitlab.com/gitlab-com/diversity-and-inclusion/-/issues/new?issuable_template=diversity-inclusion-belonging-training-template) which reviews key sections of our handbook and a learning path. We all need to work on making sure we are inclusive as we work together to build some really great things.

### Get familiar with GitLab

#### GitLab Environments

Take some time to familiarize yourself with the different [GitLab environments](/handbook/engineering/infrastructure/environments/) we have.

#### GitLab versions and tiers

GitLab is built on an open core model. That means there are two versions of GitLab: Community Edition (CE) and Enterprise Edition (EE).

GitLab Community Edition is open source, with an MIT Expat license. GitLab Enterprise Edition is built on top of Community Edition: it uses the same core, but adds additional features and functionality on top of that. This is under a proprietary license.

It is a good idea to familiarize yourself with the differences between CE and EE. The [tiers within CE and EE are listed here](/handbook/marketing/strategic-marketing/tiers/). Review the [feature comparison](/pricing/self-managed/feature-comparison/) to understand the features available within each tier.

### Enterprise Applications Team - What do we do?

The Enterprise Applications Team implements and supports specialized applications that support our business processes within GitLab.

We are directly responsible for all of GitLab's finance systems and Enterprise Applications Integrations. We build and extend these applications to support the processes of our business partners and rationalize our application landscape to ensure it is optimized for efficiency and spend.

> If you haven't already, please read the main section of the [Enterprise Applications Team Handbook](/handbook/business-ops/enterprise-applications/) page.

### Enterprise Applications Team - How do we organize our work?

#### EntApps Groups & Projects

With GitLab Groups, you can:

- Assemble related projects together.
- Grant members access to several projects at once.

The EntApps Team use mainly the parent [Enterprise Applications group](https://gitlab.com/gitlab-com/business-ops/enterprise-apps) that contains each team subgroup: [Business Systems Analysts subgroup](https://gitlab.com/gitlab-com/business-ops/enterprise-apps/bsa), [Integrations subgroup](https://gitlab.com/gitlab-com/business-ops/enterprise-apps/integrations), [Finance Operations subgroup](https://gitlab.com/gitlab-com/business-ops/enterprise-apps/financeops) and others.

Inside each subgroup, team members can create several Projects that gives them access to a large number of features. The most commonly used by BSAs are:

- Issue trackers: Used to discuss implementations with our team within issues.
- Issue Boards: Organize and prioritize our workflow.
- Merge Requests: Apply our branching strategy and get reviewed by our team.
- Merge Request Approvals: Ask for approval before implementing a change .
- Fix merge conflicts from the UI: Your Git diff tool right from GitLab’s UI.
- Labels: Organize issues and merge requests by labels.
- Milestones: Work towards a target date.
- Description templates: Define context-specific templates for issue and merge request description fields for our project.
- Slash commands (quick actions): Textual shortcuts for common actions on issues or merge requests.
- Web IDE.

> For more information about GitLab groups, see the [Groups page](https://docs.gitlab.com/ee/user/group/) from GitLab Docs.<br>
> For more information about GitLab projects, see the [Projects page](https://docs.gitlab.com/ee/user/project/) from GitLab Docs.<br>
> For a video introduction to GitLab Groups, see [GitLab University: Repositories, Projects and Groups](https://www.youtube.com/watch?v=4TWfh1aKHHw).

### Business Systems Analysts - What do we do?

Being business process first, means that the Business Systems Analysts will firm up requirements, use cases and process flows as we implement systems, enhance them or deliver fixes. In a project, the BSA will assist with Business Process Mapping, gap analysis, project scoping, planning, and scheduling.
The Business Systems Analysts are also skilled to manage implementations by working closely with cross-functional business and vendor teams to implement Enterprise Applications that are coming on board. They follow a process that ensures we keep multiple groups aligned as we iterate to get the systems up quickly and efficiently.

> If you haven't already, please read the [Business Systems Analysts Handbook](/handbook/business-ops/enterprise-applications/bsa/) page.

### Business Systems Analysts - What can we help with?

As mentioned in the above _what do we do section_, a Business Systems Analyst can be involved in different areas from Business Analysis to Implementation Management. The main tasks that a BSA is skilled to lead and/or assist are listed below, together with the artefacts that can be used to support our engagement with our clients:

#### Project-related tasks

- **Project Kick-off**

A BSA can help the Project Team to organize, schedule and prepare a project kickoff meeting. This is the first meeting between a project team and the client or sponsor of a project when kicking off a new project. It’ll take place after contracts have been signed and there’s agreement on the Statement Of Work (SoW), costs and timeline. Leverage the presence of a BSA to introduce the team, understand the project background, understand what success looks like, understand what needs to be done and agree on how to work together effectively.
> Project Kickoff meeting deck template [here](https://docs.google.com/presentation/d/1jpe-rgBroH1CGz7WXeCl1UISIrTLXDT44UjjVNPnQFk/edit?usp=sharing).

- **Executive Steering Committee meetings**

A Steering Committee is a form of corporate governance made up of high-level executives, authorities or stakeholders who provide strategic oversight and guidance to one or more projects within an organization. When these people meet, it’s called a Steering Committee Meeting. A BSA can host these meetings to assist with general guidance of the project and instructing the project/programme manager.
> Executive Steering Committee meeting template [here](https://docs.google.com/presentation/d/15dlUzy5qszw34MTTKZPnoye0P_R7vpeOv9PKlaihaMw/edit?usp=sharing).

- **Weekly Status Report**

The project weekly status reports are timely updates on the progress of a projects. They answer the questions everyone seems to be asking (before they actually ask them). Written concisely, they offer high-level information about a project, rather than every detail. BSAs can help the team build and schedule those as well as create a Geekbot automated report that will send the necessary information directly to a project slack channel.
> Weekly Status Report template [here](https://docs.google.com/presentation/d/19FjcV_PYuT7L182HSw35c33T4L7muWf52i4-7gLmrF0/edit?usp=sharing).<br>
> Geekbot stand ups guide [here](https://geekbot.com/blog/daily-standup-meeting/).

- **RAID Log**

RAID is an acronym that stands for Risks, Actions, Issues and Decisions. A RAID log is a project management tool that tracks these risks, actions, issues and decisions. It’s a simple way to organize this information and comes in handy during meetings and project audits. A BSA can help keeping the RAID log updated by engaging with the necessary stakeholders to action on their tasks.
> RAID Log template [here](https://docs.google.com/spreadsheets/d/1YeG_DO-EuR_hljl6BxSeQx_22htcjz6UKOInhecHncE/edit?usp=sharing).

#### Meetings

- **Meeting notes**

A BSA can help taking notes, recording meetings as necessary and storing them into a secure location.
> External Meetings template [here](https://docs.google.com/document/d/1b0MEsjn27n_-7njXE0uC822Rhl4jkMM-Mu1r2siFij8/edit?usp=sharing).<br>
> Internal Meetings template [here](https://docs.google.com/document/d/1LG_KOzVJHCrxkMzCo9F_1UBFINwr7cTAdulvt05Ttv0/edit?usp=sharing).

#### Information gathering

- **User Stories**

A User Story is an informal, general explanation of a software feature written from the perspective of the end user. The purpose of a user story is to articulate how a piece of work will deliver a particular value back to the customer. Leverage the assistance of a BSA to collect ant track those.
> User Stories template [here](https://docs.google.com/spreadsheets/d/17AGafZAEOCdZTPIxy4mgrGlNAJ2Bo7NWjLkPebwnFY8/edit?usp=sharing).

- **Requirements**

Requirements are conditions or capabilities that are required to be present in a product, service or result to satisfy a contract or other formally imposed specification. The purpose of stakeholder requirements is to describe the needs of a stakeholder or stakeholder group. Leverage the assistance of a BSA to collect ant track those.
> Requirements template [here](https://docs.google.com/spreadsheets/d/1nylZN30o8urNj77JnmIRLpjgY7gbf0JR2IOPx8JRYAo/edit?usp=sharing).

#### Testing

- **UAT (User Acceptance Testing) and CRP (Conference Room Pilot)**

The purpose of the CRP is to validate a software application against the business processes of end-users of the software, by allowing end-users to use the software to carry out typical or key business processes using the new software. UAT however, verifies whether the software works for the user (i.e. whether the user accepts how the software solves a problem). BSAs can help with documenting and prioritizing any issues resulting from testing, so it can be resolved by the appropriate party.
> Testing Tracker template [here](https://docs.google.com/spreadsheets/d/1dehsoFK1wsPY1QVV4GAc0R6XS08OdBOt0b2vZBk_2oU/edit?usp=sharing).

### Business Systems Analysts - How do we organize our work?

#### BSA Groups & Projects

As mentioned above, the [Business Systems Analysts subgroup](https://gitlab.com/gitlab-com/business-ops/enterprise-apps/bsa) is hosted by the [Enterprise Applications parent group](https://gitlab.com/gitlab-com/business-ops/enterprise-apps). Inside this subgroup, there are projects that are related to the work of BSAs however, a BSA usually work in the groups and projects of the team/department they are assisting.

Anyone can create a project. If you need to create a new one to relate to the work you are/will be doing, you can follow this guide on [how to create a project](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

#### GitLab issues
As you begin to get settled in, you will most likely need to create or update an issue.

Workflow for creating an issue:
* Visit the issues list from the [GitLab.com group](https://gitlab.com/groups/gitlab-com/-/issues) or [GitLab.org group](https://gitlab.com/groups/gitlab-org/-/issues).
* Search to make sure the issue doesn't already exist.
* Navigate to the project you want to add the issue to, click **New Issue**.
* In general, most BSA issues start in the [Business Systems Analysts project](https://gitlab.com/gitlab-com/business-ops/enterprise-apps/bsa/business-systems-analysts).
* Choose a template from the **Choose a template** dropdown and take a look at the **typical kinds of issues created** below.
* Fill in all the relevant sections.
* `@mention` someone in the issue to attract attention to it. Choose an expert [here](/company/team/) or feel free to ask in the #business-technology channel on Slack who it should be reviewed by. Do not worry that you are poking someone to review something when you don't even know them and they might be much more senior than you, if it's not appropriate for them, they will know the right person to pass it along to and do that.

Typical kinds of BSA issues created:

- New Projects: when the BSA will be involved in a already established project.
    - Template "New Project" [here](https://gitlab.com/gitlab-com/business-ops/Business-Operations/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).
- Project Proposal: when the BSA has been dealing with different teams and a new project has to be organized.
    - Template "Project Proposal" [here](https://gitlab.com/gitlab-com/business-ops/Business-Operations/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=).

## Relevant links

- [Business Operations Handbook](/handbook/business-ops/)
- [Enterprise Applications Handbook](/handbook/business-ops/enterprise-applications/)
- [Business Systems Analysts Handbook](/handbook/business-ops/enterprise-applications/bsa/)
- [Business Technology Group](https://gitlab.com/gitlab-com/business-ops)
- [Enterprise Applications Project](https://gitlab.com/gitlab-com/business-ops/enterprise-apps)
