---
layout: handbook-page-toc
title: "Customer Digital Journey"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


### Digital Journey

The digital journey is designed to enable customers to better self-service the educational and “best practices” consulting that Technical Account Managers (TAMs) provide in the [TAM-Assigned segment](/handbook/customer-success/tam/customer-segments-and-metrics/#tam-assigned-segment). This material is designed to help speed up time-to-value with GitLab, and to help customers adopt workflows and best practices more quickly in order to deepen their return on investment.

Digital journey content is an extension of our existing documentation and community enablement materials ([YouTube](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg), [GitLab blog](https://about.gitlab.com/blog/), [case studies](https://about.gitlab.com/customers/), [forums](https://forum.gitlab.com/), [docs site](https://docs.gitlab.com/) ) designed to help organize the most relevant and most useful resources and help implementation engineers quickly become experts with advice taken from hundreds of existing customers’ experiences and unique environmental or business requirements.  The digital journey takes 30 days to complete and is followed by a survey asking for feedback on the series.

### Digital Onboarding

The following topics are covered through the digital onboarding series:
1. Quick guide intro and authentication
1. Setting up Group and Project hierarchy
1. Import/Export
1. GitLab Instance - Security Best Practices
1. Monitoring GitLab
1. Backing Up GitLab
1. How we guarantee backups (GitLab.com)
1. Alternative Backup Strategies
1. Support information (self)
1. Support information (GitLab.com)
1. API Intro + Rate limits (self)
1. API Intro + Rate limits (GitLab.com)
1. Where to get training

The [digital onboarding email content](https://docs.google.com/document/d/1nOkcmbzKSsaLdJwZquGG8otrMHeZpqpYRTOmhw2kEYE/edit?usp=sharing) can be viewed here (GitLab only)


**Digital Journey** - [Epic](https://gitlab.com/groups/gitlab-com/customer-success/-/epics/65)

The email templates will leverage existing materials from YouTube, the GitLab blog, GitLab docs, and other GitLab and 3rd party resources to help customers. However a strong emphasis needs to be placed on making these materials easy for a busy DevSecOps team to consume quickly. As such some longer form materials may need to be adapted to a shorter format for best results.

### Roadmap

1. CI enablement (Verify Stage) [epic](https://gitlab.com/groups/gitlab-com/customer-success/-/epics/69) for those customers who through product analytics are deemed to be using CI
1. Security enablement (epic pending, Secure Stage)
1. CD enablement (Release Stage)

### How To Use

Instructions on adding contacts to the digital onboarding journey can be found [here](/handbook/customer-success/tam/digital-journey/nominating-contacts-for-the-digital-journey/)

TAMs who wish to send Digital Journey emails to their customers have 2 methods:

#### Gainsight Assist gmail plugin  
1.  Install the Chrome plugin [Gainsight Assist](https://chrome.google.com/webstore/detail/gainsight-assist/kbiepllbcbandmpckhoejbgcaddcpbno)  
1.  Configure the plugin domain (just “gitlab” of gitlab.gainsight.com) and login  
1.  Choose “Compose an email” from the Gainsight plugin panel in Gmail  
1.  Search for the template you wish to send  
1.  (optional) Enter Primary Recipient from Gainsight contacts. This will auto-complete any other tokens in the email template based on the Company record for each contact.  
1.  Click “Apply Template” to have a draft email populated in Gmail, ready for you to send to the customer.  

#### CTA with a [Playbook](/handbook/customer-success/tam/gainsight/#ctas)
Some emails intended to be sent as a sequence have been added as Playbooks in Gainsight. These need to be sent manually by the TAM, but are setup to facilitate reminders and make this process easy, while we learn how customers want to engage. To use a playbook to send emails:
1.  In Gainsight, create a new CTA
1.  Type: Lifecycle, choose the Playbook (example:  Commercial Onboarding Email Series). This creates a new CTA with a checklist for each email in the sequence.
1.  Click the whitespace next to the name of the next email to be sent, then on “Validate Email”.
1.  This opens an editor where you can choose contacts and modify the email before sending it.
1.  Click “Preview and Proceed” to see the fully populated and formatted email before sending.
1.  Then send the email (or save as draft).   

To request new materials or capabilities, please use the Commercial-Markets-Initiatives board and CC `@pharlan` for scheduling.
