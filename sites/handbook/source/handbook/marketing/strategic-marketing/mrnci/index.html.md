---
layout: handbook-page-toc
title: "Market Research and Customer Insights"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Market Research and Customer Insights at GitLab

**Who We Are**
- A Team of Experienced Customer Reference and Industry Analyst Relations Professionals

**What We Do**
- Gather & Build - Information & Relationships
- Distill - Impactful Insights
- Activate - Turning Insights Into Action

**How We Do It**
- [Customer Reference Program](/handbook/marketing/strategic-marketing/customer-reference-program/)
  - Customer Advisory Board
  - [Customer Case Studies](https://about.gitlab.com/customers/)


- [Peer Review Site Management](/handbook/marketing/strategic-marketing/customer-reference-program/peer-reviews/) (Gartner Peer Insights, G2, Trust Radius, etc.)


- [Industry Analyst Relations (IAR)](/handbook/marketing/strategic-marketing/analyst-relations/)
  - Interactions (Briefings, Inquiry, Strategic Advisory)
  - Publications (Comparative Research, Market Landscape, Best Practices, Thought Leadership)
  - View the [Analyst Relations Issue Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/940099?&label_name[]=Analyst%20Relations) and [AR-Cats Issue Board](https://gitlab.com/gitlab-com/marketing/product-marketing/-/boards/940116?&label_name[]=Analyst%20Relations) to see what's currently in progress.

### Which Market Research and Customer Insights team member should I contact?

  - Listed below are areas of responsibility within the Market Research and Customer Insight team:

    - [Ryan](/company/team/#ryanragozzine), Analyst Relations Manager
    - [Fiona](/company/team/#fokeeffe), Reference Program Manager; Regional Lead: EMEA and APAC; Program Lead: Customer Advisory Board, Customer Speaking Requests, Industry Analyst Relations Requests
    - [Jen](/company/team/#jlparker), Reference Program Manager; Regional Lead: Americas; Program Lead: Customer Insights/Case Studies, Peer Reviews, ReferenceEdge
    - [Colin](/company/team/#colinwfletcher), Manager, Market Research and Customer Insights

#### Customer Reference KPI's
  - Quantity of Case Studies published per month - 2 case studies published per month
