---
layout: handbook-page-toc
title: Contribute to GitLab Learn
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}


**Important Note:** As of 2021-01-07, this process is in the `draft` state. The GitLab Learn platform has not yet been launched to the wider GitLab community. Upon the launch, this process will be further finalized. If you have contributions that you think would improve this draft process, please submit a merge request.


### Step 1: Create and Curate Learning Content

1. Make a contribution to the handbook. All learning content in the LXP is built with a handbook first approach. This means that information you'd like to add to the LXP must be stored first in the handbook. If you have not made a contribution to the GitLab handbook before, please review [how to contribute to GitLab](https://about.gitlab.com/community/contribute/). 

The table below should be used as a reference for suggested handbook sections where learning content can be uploaded. If you aren't sure where to contribute learning content in the handbook, please review the [main handbook page](https://about.gitlab.com/handbook/#introduction) or contact the Learning and Development team at `learning@gitlab.com` for support.

| Content | Suggested Handbook Location |
| ----- | ----- |
| Leadership: emotional intelligence, coaching, and all other factors that contribute to growing and developing as a leader at GitLab | [Leadership Handbook](https://about.gitlab.com/handbook/leadership/) |
| Communication: effective communication strategies, remote communication, and any additional content that contributes to strengthening asynchronous and remote communication skills | [Communication Handbook](https://about.gitlab.com/handbook/communication/) |
| Diversity, Inclusion, and Belonging: inclusivity trainings, strategies for developing a sense of belonging, and content related to implementing DIB initiatives | [DIB Handbook](https://about.gitlab.com/company/culture/inclusion/) | 

1. If the content you'd like to contribute to the LXP is already included in the handbook - great! Your contribution to the LXP could be a new or creative curation of existing content from GitLab that helps a learner better understand a topic or reach a goal. Consider curating current content from the handbook, [GitLab YouTube channel](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg), and [GitLab documentation](https://docs.gitlab.com/). Consider taking the opportunity to add to or update existing content before contributing to the LXP.

### Step 2: Propose Content to be included in the LXP

1. Using the descriptions above, decide which content type might be the best fit for the information you'd like to contribute. Navigate to the corresponding creator kit issue templates below and begin to populate the issue with required content.

Issue Templates:

- SmartCard Creator Kit
- Pathway Creator Kit
- Journey Creator Kit
- Carousel Creator Kit
- Channel Creator Kit
- Group Creator Kit

**Note:** In order for content contributions to be reviewed by the appropriate GitLab team, all information on the issue template must be addressed. GitLab team members may ping you to address content gaps or answer questions about your contributions. 

### Step 3: Iterate on content with GitLab team via issues

1. After all required content is uploaded to the correct creator kit issue, GitLab team members will collaborate with contributors to iterate on and refine contributions. The following chart highlights which GitLab teams will collaborate on and approve content contributed to the LXP:

| Contributor Audience | Team Approval | GitLab Label|
| ----- | ----- | ----- |
| GitLab Team Members | L&D Team | `edcast-contribution-l&D` |
| Customers | Professional Services | `edcast-contribution-professional-services` |
| Partners | Partner Enablement | `edcast-contribution-partners` |
| Sales Team | Field Enablement | `edcast-contribution-field` |
| Wider GitLab Community | Community Relations Team | `edcast-contribution-community` |


To learn more about the admin review and approval process of contributions to the LXP, please review the LXP admin section of the handbook.

1. The GitLab team will determine when your contributions are ready to be added to the LXP and will communicate with contributors on related issues.


### Step 4: Content is uploaded to the LXP

1. The GitLab team members outlined above will upload related content to the LXP. Additional context about this admin process can be found in the LXP admin section of the handbook.

**Note:** At the GitLab team's discretion, individual contributors may be given permissions to upload content to the LXP.