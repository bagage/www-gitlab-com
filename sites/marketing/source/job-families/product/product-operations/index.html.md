---
layout: job_family_page
title: "Product Operations"
---

## Principal Product Manager, Product Operations

### Role

The mission of the GitLab product team is to consistently create products and experiences that customers love and value.  To do this consistently, we strive to create a world class product system in which the extended product team can do the best work of their careers.  GitLab is seeking a Sr. Product Manager to manage the GitLab product development system as a product, and help evolve it into a world class system that can scale rapidly with GitLab the company.

### Responsibilities
- **Evolve and refine the product management system**
  - Own the definition and execution of the GitLab [product development flow](https://about.gitlab.com/handbook/product-development-flow/)
  - Recommend and implement optimizations to the [product development timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline)
  - Drive content and training to uplevel the skills of the product team
  - Maintain the [Product Handbook](/handbook/product)
- **Automate internal and external communication**
  - Automate communication of the [Direction](/handbook/product/product-management/process/#section-and-stage-direction) and [Vision](/handbook/product/product-management/process/#category-direction) pages
  - Ensure smooth communication during the monthly [kick-off](/handbook/product/product-management/process/#kickoff-meetings)
  - Participate in the [creation of external content](/handbook/product/product-processes/#communication#writing-about-features) including blogs, webinars, and demos
  - Create visually compelling and interactive [roadmap](/handbook/product/#3-month-roadmap) views
  - Implement standard processes for [internal evangelism](/handbook/product/product-processes/#communication#internal-and-external-evangelization)
- **Facilitate improved product input from stakeholders**
  - Design and build systems to increase stakeholder [feedback](/handbook/product/how-to-engage/#how-do-i-share-feedback) in GitLab issues
  - Develop intelligent tools for collecting and aggregating larger volumes of customer feedback

### Requirements

- 5+ years of overall experience in either Product Management or Project/Program Management roles
- Experience in customer-first product systems similar to GitLab's, especially important is familiarity with modern customer validation techniques
- Web development skill and familiarity with Git preferred
- You are living wherever you want and are excited about the [all remote](https://about.gitlab.com/company/culture/all-remote/) lifestyle
- You share our [values](/handbook/values/), and work in accordance with those values
- Ability to use GitLab

## Group Manager, Product Operations

### Role

As the Group Manager, Product Operations, you will be responsible for ensuring the broader Product Management team is operating at a high level.  Your "customers" will be a variety of internal constituents, including the VP, Product, the CEO, the Product Management team, and other cross-functional departments.  This role reports to the VP, Product.

### Individual responsibility

- Make sure you have a great product operations team (recruit and hire, sense of progress, promote proactively, identify underperformance)
- Collaborate on the group's charter with the VP of Product Management, EVP of Product, and CEO; and communicate this charter cross-functionally
- Work closely across the company with Product Management, Engineering, Design, Sales, Customer Success, etc.

### Team responsibility

- Serve as "Chief of Staff" for the EVP, Product, attending all key Product Management leadership meetings and helping to ensure execution of Product team decisions
- Help define, document, and ensure consistent execution of the GitLab product management / product development process
- Manage a team of 2-4 product managers and/or product operations specialists

### Requirements

* 3+ years in Product Management
* 1+ years of people management experience
* Past experience in project management, program management and/or product operations preferred
* Experience in companies that employ modern product management & software development techniques preferred
* You are living wherever you want and are excited about the [all remote](https://about.gitlab.com/company/culture/all-remote/) lifestyle
* You share our [values](/handbook/values/), and work in accordance with those values
