---
layout: markdown_page
title: "MicroFocus Fortify"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## Summary

Both Fortify and GitLab Ultimate offer open source component scanning along with Static and Dynamic Application Security Testing. Fortify uses Sonatype for open source scanning in its SaaS product as an OEM service. Fortify on-premise is integrated with SonaType and BlackDuck for open source scanning on-premise, both of those require separate licensing and set up.

Fortify is a mature product and has led the Gartner Magic Quadrant for application security testing every year. Fortify offers IAST (with DAST) and RASP, though it does not offer container scanning. In spite of it's strengths as a stand-alone product, Fortify is a separate process outside of the developer's merge request workflow. The Fortify RASP product, Application Defender, is limited to Java and .Net applications. The Fortify Security Center (SSC) is needed if you want to pull results together from across the various Fortify scanners. Fortify acquired WebInspect many years back for DAST but you may still hear the name WebInspect from customers.  Fortify does not offer any form of container scanning currently.  Fortify does not offer a portfolio level dashboard of vulnerabilities.

Fortify can be easily integrated into GitLab CI process using Command Line and API integrations. A key difference is that Fortify does not yet provide incremental scanning while GitLab clearly shows the scan results based upon the diff, or code changed since the last commit.

GitLab automatically includes broad security scanning with every code commit including Static and Dynamic Application Security Testing, along with dependency scanning, container scanning, and license management.  Results are provided to the developer in the CI pipeline (Merge Request) with no integration required. GitLab also aggregates and displays all results in a single dashboard both at Group and Project levels.

* GitLab security scanning includes not only SAST but also DAST, Container and Dependency scanning, License Compliance scanning, and Secrets detection. All of these are included in GitLab Ultimate and integrated directly into the developer's workflow.
* Finding vulnerabilities is only the beginning. Delivering those findings to the developer for immediate remediation is key to shifting left to reduce both cost and risk. GitLab does so without added integrations to maintain.


## Comparison

